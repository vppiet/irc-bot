from ruuvitag_sensor.ruuvitag import RuuviTag
import sys

class RuuviTagCaller (object):

    def __init__(self, mac_address):
        self.ruuvitag = RuuviTag(mac_address)

    def get_latest_state(self):
        state = self.ruuvitag.update()
        try: 
            temperature = state.get('temperature')
            pressure = state.get('pressure')
            return "Temperature: %.1f ºC, pressure: %.1f hPa" % (temperature, pressure)
        except TypeError:
            return "No value from RuuviTag, maybe dead battery?"

if __name__ == '__main__':
    ruuviTagCaller = RuuviTagCaller(sys.argv[1])
    print(ruuviTagCaller.get_latest_state())
