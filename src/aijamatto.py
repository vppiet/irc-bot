import requests
from bs4 import BeautifulSoup

class AijaMatto (object):

	def get_data(self):
		URL = "http://aijamatto.herokuapp.com/"

		response = requests.get(url = URL)

		if (response.status_code == 200):
			html_response = response.text
			soup = BeautifulSoup(html_response, 'html.parser')
			return soup.body.div.text

		else:
			return "Something went wrong."
